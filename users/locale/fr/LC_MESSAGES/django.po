# SugarCUB users app fr localisation file.
# Copyright (C) 2015 Matthias Devlamynck
# This file is distributed under the same license as the SugarCUB package.
# Matthias Devlamynck <matthias.devlamynck@mailoo.org>, 2015
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: SugarCUB 0.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-03-12 11:55+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Matthias Devlamynck <matthias.devlamynck@mailoo.org>\n"
"Language: French\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: users/admin.py:20
#, python-format
msgid "%s profiles were enabled."
msgstr "%s profils ont été activés."

#: users/admin.py:24
#, python-format
msgid "%s profiles were disabled."
msgstr "%s profils ont été désactivés."

#: users/admin.py:26
msgid "Enable the selected profiles"
msgstr "Active les profils sélectionnés"

#: users/admin.py:27
msgid "Disable the selected profiles"
msgstr "Désactive les profils sélectionnés"

#: users/apps.py:7
msgid "Users"
msgstr "Membres"

#: users/forms.py:27
msgid "Exemple : Pony is best pegasus/pony/unicorn!"
msgstr "Exemple: Pony est le meilleur pégase / poney / licorne !"

#: users/forms.py:40
msgid "Birthday"
msgstr "Date de naissance"

#: users/forms.py:50
msgid "Biography"
msgstr "Biographie"

#: users/forms.py:51
msgid "Phone"
msgstr "Téléphone"

#: users/forms.py:52
msgid "Address"
msgstr "Adresse"

#: users/forms.py:53
msgid "City"
msgstr "Ville"

#: users/forms.py:54
msgid "Postal code"
msgstr "Code postal"

#: users/forms.py:55
msgid ""
"Show my firstname and lastname (When hidden only your pseudo will be visible)"
msgstr "Montrer mon prénom et nom (Si caché seul votre pseudo sera visible)."

#: users/forms.py:56
msgid "Show my phone number (When hidden your will only be rechable by email"
msgstr ""
"Montrer mon numéro de téléphone (Si caché vous ne serez joignable que par "
"email)."

#: users/forms.py:57
msgid ""
"Show my birthday (When hidden nobody will know your age or your birthday)"
msgstr ""
"Montrer ma date de naissance (Si caché personne ne connaîtra ni votre âge ni "
"votre anniversaire)."

#: users/forms.py:58
msgid "Show my address (When hidde nyou will not appear on the members map"
msgstr ""
"Montrer mon adresse (Si caché vous n'apparaîtrez pas sur la carte des "
"membres)."

#: users/forms.py:59
msgid "Show my email"
msgstr "Montrer mon adresse mail."

#: users/forms.py:65
msgid ""
"Write about yourself! how you discover My Little Pony or BronyCUB, what you "
"like and dislike..."
msgstr ""
"Ecrivez à propos de vous ! Comment vous avez découvert My Little Pony ou "
"BronyCUB , ce que vous aimez ou detestez ..."

#: users/forms.py:100
msgid "Username or email"
msgstr "Nom d'utilisateur ou email"

#: users/models.py:43
msgid ""
"Phone number must be entered in the format: '+999999999'. Up to 15 digits "
"allowed."
msgstr ""
"Le numéro de téléphone doit être entré dans le format : '+999999999'. "
"Jusqu'à 15 chiffres sont autorisés"

#: users/models.py:45
msgid "The postal code must be 5 digits."
msgstr "Le code postal doit être de 5 chiffres"

#: users/models.py:47
msgid "user"
msgstr ""

#: users/models.py:49
msgid "enabled"
msgstr "accepté"

#: users/models.py:50
msgid "name enabled"
msgstr "nom affiché"

#: users/models.py:51
msgid "mail enabled"
msgstr "email affiché"

#: users/models.py:54
msgid "The bio should be longer than 150 character"
msgstr "La bio doit être longue de plus de 150 caractère"

#: users/models.py:55
msgid "bio"
msgstr "biographie"

#: users/models.py:56
msgid "avatar"
msgstr ""

#: users/models.py:61
msgid "phone"
msgstr "téléphone"

#: users/models.py:62
msgid "phone enabled"
msgstr "téléphone affiché"

#: users/models.py:64
msgid "birthday"
msgstr "date de naissance"

#: users/models.py:65
msgid "birthday enabled"
msgstr "date de naissance affichée"

#: users/models.py:67
msgid "address"
msgstr "adresse"

#: users/models.py:68
msgid "city"
msgstr "ville"

#: users/models.py:69
msgid "postal code"
msgstr "code postal"

#: users/models.py:70
msgid "address enabled"
msgstr "adresse affichée"

#: users/models.py:72
msgid "longitude"
msgstr ""

#: users/models.py:73
msgid "latitude"
msgstr ""

#: users/models.py:76 users/models.py:134
msgid "profile"
msgstr "profil"

#: users/models.py:77
msgid "profiles"
msgstr "profils"

#: users/models.py:106 users/models.py:120
msgid "name"
msgstr "nom"

#: users/models.py:107 users/models.py:121
msgid "file name"
msgstr "nom du fichier"

#: users/models.py:110
msgid "pony icon"
msgstr "image de poney"

#: users/models.py:111
msgid "pony icons"
msgstr "images de poney"

#: users/models.py:124
msgid "site icon"
msgstr "icône de site"

#: users/models.py:125
msgid "site icons"
msgstr "icônes de site"

#: users/models.py:135 users/models.py:139
msgid "pony"
msgstr "poney"

#: users/models.py:136
msgid "message"
msgstr "message"

#: users/models.py:140
msgid "ponies"
msgstr "poneys"

#: users/models.py:152
msgid "profil"
msgstr "profil"

#: users/models.py:153 users/models.py:157
msgid "url"
msgstr "url"

#: users/models.py:154
msgid "icon"
msgstr "icône"

#: users/models.py:158
msgid "urls"
msgstr "urls"

#: users/templates/members.html:5
msgid "Members"
msgstr "Membres"

#: users/templates/members.html:16
msgid "No members registered yet..."
msgstr "Aucun membre enregistré pour le moment ..."

#: users/templates/members/one-user-modal.html:64
msgid ""
"Your account must be accepted by the admin to see the members informations."
msgstr ""
"Votre compte doit être validé par un administrateur pour avoir accès aux "
"informations des autres membres"

#: users/templates/members/one-user-modal.html:67
#: users/templates/members/one-user-modal.html:78
msgid "Close"
msgstr "Fermer"

#: users/templates/members/one-user-modal.html:75
msgid "You must be connected to see the members informations."
msgstr "Vous devez être connecté pour voir les informations de ce membre."

#: users/templates/profile.html:7
msgid "Profile"
msgstr "Profil"

#: users/templates/profile.html:22
msgid "My Profile"
msgstr "Mon Profil"

#: users/templates/profile.html:39
msgid "Favorite pony :"
msgstr "Poney Favoris :"

#: users/templates/profile.html:57 users/templates/profile.html:79
msgid "Add new line"
msgstr "Ajouter une nouvelle ligne"

#: users/templates/profile.html:61
msgid "Url :"
msgstr "Url :"

#: users/templates/profile.html:83
msgid "Only members of BronyCUB will be able to see those information :"
msgstr "Seul les membres de BronyCUB peuvent voir les informations suivantes :"

#: users/templates/profile.html:94
msgid "Hide personnal information :"
msgstr "Cacher vos informations personnelles"

#: users/templates/profile.html:105
msgid "Update"
msgstr "Mettre à jour"

#: users/templates/profile.html:110
msgid "Change your password"
msgstr "Modifiez votre mot de passe"

#: users/templates/registration/activate.html:5
#: users/templates/registration/activation_complete.html:6
#: users/templates/registration/logout.html:6
#: users/templates/registration/registration_closed.html:5
#: users/templates/registration/registration_complete.html:5
msgid "Account created"
msgstr "Compte créé"

#: users/templates/registration/activate.html:13
msgid "An error occured while activating your account. Please retry."
msgstr ""
"Une erreur est survenue pendant l'activation de votre compte. Veuillez "
"réessayer."

#: users/templates/registration/activation_complete.html:14
msgid "Your account is now activated ! Please login and enjoy ! ;)"
msgstr ""
"Votre compte est maintenant activé ! Veuillez vous connecter et profiter ! ;)"

#: users/templates/registration/activation_email.txt:4
#, python-format
msgid "You're receiving this email because you created an account on %(site)s."
msgstr "Vous recevez ce mail parce que vous avez créé un compte sur %(site)s."

#: users/templates/registration/activation_email.txt:6
msgid "Please go to the following page to activate your account:"
msgstr "Veuillez vous rendre sur cette page pour activer votre compte :"

#: users/templates/registration/activation_email.txt:11
#: users/templates/registration/password_reset_email.html:10
msgid "Thanks for using our site!"
msgstr "Merci d'utiliser notre site !"

#: users/templates/registration/activation_email.txt:13
#, python-format
msgid "The %(site)s team"
msgstr "L'équipe de %(site)s"

#: users/templates/registration/activation_email_subject.txt:3
msgid "Account activation"
msgstr "Activation du compte"

#: users/templates/registration/email-admin-notification.html:3
msgid "A new user just registered."
msgstr "A nouvel utilisateur vient de s'inscrire."

#: users/templates/registration/forms/login.html:6
#: users/templates/registration/forms/login.html:9
#: users/templates/registration/login.html:7
msgid "Login"
msgstr "Connexion"

#: users/templates/registration/forms/login.html:12
msgid "You forgot your password ?"
msgstr "Vous avez oublié votre mot de passe ?"

#: users/templates/registration/forms/login.html:14
#: users/templates/registration/registration_form.html:7
msgid "Signup"
msgstr "Nous rejoindre"

#: users/templates/registration/informations.html:6
msgid "To know before registering"
msgstr "À savoir avant de s'inscrire"

#: users/templates/registration/logout.html:14
msgid "You're now logged out."
msgstr "Vous êtes maintenant déconnecté."

#: users/templates/registration/password_reset_done.html:6
msgid "Home"
msgstr "Accueil"

#: users/templates/registration/password_reset_done.html:7
msgid "Password reset"
msgstr "Réinitialisation du mot de passe"

#: users/templates/registration/password_reset_done.html:11
#: users/templates/registration/password_reset_done.html:12
msgid "Password reset successful"
msgstr "Mot de passe réinitialisé"

#: users/templates/registration/password_reset_done.html:17
msgid ""
"We've emailed you instructions for setting your password, if an account "
"exists with the email you entered. You should receive them shortly."
msgstr ""
"Nous vous avons envoyé par email les instructions pour changer de mot de "
"passe, pour autant qu'un compte existe avec l'adresse que vous avez "
"indiquée. Vous devriez recevoir rapidement ce message."

#: users/templates/registration/password_reset_done.html:18
msgid ""
"If you don't receive an email, please make sure you've entered the address "
"you registered with, and check your spam folder."
msgstr ""
"Si vous ne recevez pas de message, vérifiez que vous avez saisi l'adresse "
"avec laquelle vous vous êtes enregistré et contrôlez votre dossier de spam."

#: users/templates/registration/password_reset_email.html:2
#, python-format
msgid ""
"You're receiving this email because you requested a password reset for your "
"user account at %(site_name)s."
msgstr ""
"Vous recevez ce mail parce que vous avez demandé à réinitialiser votre mot "
"de passe sur %(site_name)s."

#: users/templates/registration/password_reset_email.html:4
msgid "Please go to the following page and choose a new password:"
msgstr ""
"Veuillez vous rendre sur cette page et choisir un nouveau mot de passe :"

#: users/templates/registration/password_reset_email.html:8
msgid "Your username, in case you've forgotten:"
msgstr "Votre nom d'utilisateur, si jamais vous l'avez oublié :"

#: users/templates/registration/password_reset_email.html:12
#, python-format
msgid "The %(site_name)s team"
msgstr "L'équipe de %(site_name)s"

#: users/templates/registration/registration_closed.html:13
msgid "Sorry but registrations are closed at the moment, try again later /)"
msgstr ""
"Désolé mais pour le moment les inscriptions sont fermées, veuillez réessayer "
"plus tard /)"

#: users/templates/registration/registration_complete.html:14
msgid ""
"You signed up successfully ! A mail to activate your account has been sent, "
"Welcome !"
msgstr ""
"Vous avez bien créé votre compte ! Un mail d'activation vient de vous être "
"envoyé, Bienvenue !"

#: users/templates/registration/registration_form.html:35
msgid ""
"<strong>Important</strong> : Bien que vos informations ne sont accessibles "
"que aux membres du collectif, les options ci-dessous vous offre la "
"possibilité de vous rendre anonyme. Cela signifiera alors que vos amis ne "
"pourrons pas avoir accès ces informations et seront par conséquent très "
"tristes."
msgstr ""

#: users/templates/registration/registration_form.html:42
msgid "Send"
msgstr "Soumettre"

#: users/urls.py:8
msgid "^members$"
msgstr "^membres$"

#: users/urls.py:9
msgid "^profile$"
msgstr "^profil$"

#: users/urls.py:10
msgid "^register$"
msgstr "^inscription$"

#: users/urls.py:11
msgid "^register/informations$"
msgstr "^inscription/informations$"

#: users/utils.py:31
msgid "New registration"
msgstr "Nouvelle inscription"

#~ msgid "url icon"
#~ msgstr "icône de lien"
