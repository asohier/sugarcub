# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-03-12 11:55+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: agenda/apps.py:7
msgid "Agenda"
msgstr ""

#: agenda/forms.py:17
msgid "Start date and hour"
msgstr ""

#: agenda/forms.py:26
msgid "End date and hour"
msgstr ""

#: agenda/forms.py:33
msgid "Title"
msgstr ""

#: agenda/forms.py:34
msgid "Description"
msgstr ""

#: agenda/forms.py:35
msgid "Address of the event"
msgstr ""

#: agenda/forms.py:36
msgid "Means of contact"
msgstr ""

#: agenda/forms.py:42
msgid ""
"Explain the event, add as much information you can : time, localisation , if "
"people need stuff... the more information the better!"
msgstr ""

#: agenda/forms.py:55
msgid "Add a comment about the event..."
msgstr ""

#: agenda/models.py:9
msgid "Show both my phone and my mail"
msgstr ""

#: agenda/models.py:10
msgid "Show only my mail"
msgstr ""

#: agenda/models.py:11
msgid "Show only my phone"
msgstr ""

#: agenda/models.py:18 agenda/models.py:65 agenda/models.py:88
msgid "user"
msgstr ""

#: agenda/models.py:20
msgid "title"
msgstr ""

#: agenda/models.py:21
msgid "description"
msgstr ""

#: agenda/models.py:22
msgid "date begin"
msgstr ""

#: agenda/models.py:23
msgid "date end"
msgstr ""

#: agenda/models.py:24
msgid "address"
msgstr ""

#: agenda/models.py:25
msgid "event enabled"
msgstr ""

#: agenda/models.py:29 agenda/models.py:67 agenda/models.py:91
msgid "event"
msgstr ""

#: agenda/models.py:30
msgid "events"
msgstr ""

#: agenda/models.py:66 agenda/models.py:89
msgid "unregistered user"
msgstr ""

#: agenda/models.py:69
msgid "text"
msgstr ""

#: agenda/models.py:70
msgid "date"
msgstr ""

#: agenda/models.py:74
msgid "comment"
msgstr ""

#: agenda/models.py:75
msgid "comments"
msgstr ""

#: agenda/models.py:90
msgid "contact"
msgstr ""

#: agenda/models.py:95
msgid "participation"
msgstr ""

#: agenda/models.py:96
msgid "participations"
msgstr ""

#: agenda/templates/agenda/comment_list.html:6
#: agenda/templates/agenda/event_form.html:7
#: agenda/templates/agenda/event_list.html:8
#: agenda/templates/agenda/event_update_form.html:7
msgid "Events"
msgstr ""

#: agenda/templates/agenda/email-admin-notification.html:3
msgid "A event was just registered."
msgstr ""

#: agenda/templates/agenda/email-admin-notification.html:5
msgid "created"
msgstr ""

#: agenda/templates/agenda/event_form.html:17
#: agenda/templates/agenda/event_list.html:40
msgid "Add an event"
msgstr ""

#: agenda/templates/agenda/event_form.html:34
#: agenda/templates/agenda/event_update_form.html:23
#: agenda/templates/agenda/event_update_form.html:25
msgid ""
"You can use the button on the right of this field to choose the date and the "
"time"
msgstr ""

#: agenda/templates/agenda/event_form.html:40
msgid "Add event"
msgstr ""

#: agenda/templates/agenda/event_list.html:16
#: agenda/templates/agenda/event_list.html:22
msgid "Add to your agenda"
msgstr ""

#: agenda/templates/agenda/event_list.html:25
msgid "To add the agenda in your favorite software use the address:"
msgstr ""

#: agenda/templates/agenda/event_list.html:32
msgid "Close"
msgstr ""

#: agenda/templates/agenda/event_list.html:110
msgid "Erreur : Vous participez déjà à cet évènement!"
msgstr ""

#: agenda/templates/agenda/event_list.html:168
msgid ""
"Erreur : Vous participez déjà à cet évènement! Si ce n'est pas vous, "
"quelqu'un à déjà pris ce pseudo!"
msgstr ""

#: agenda/templates/agenda/event_list_page.html:23
msgid "From"
msgstr ""

#: agenda/templates/agenda/event_list_page.html:24
#: agenda/templates/agenda/event_list_page.html:26
msgid "At"
msgstr ""

#: agenda/templates/agenda/event_list_page.html:25
msgid "To"
msgstr ""

#: agenda/templates/agenda/event_list_page.html:27
msgid "Address"
msgstr ""

#: agenda/templates/agenda/event_list_page.html:48
msgid "Edit event"
msgstr ""

#: agenda/templates/agenda/event_list_page.html:53
msgid "Will you participate?"
msgstr ""

#: agenda/templates/agenda/event_list_page.html:64
msgid "Organizer"
msgstr ""

#: agenda/templates/agenda/event_list_page.html:74
msgid "CONTACT"
msgstr ""

#: agenda/templates/agenda/event_update_form.html:15
msgid "Update an event"
msgstr ""

#: agenda/templates/agenda/event_update_form.html:30
msgid "Update"
msgstr ""

#: agenda/templates/agenda/participation_list_page.html:8
msgid "Participant"
msgstr ""

#: agenda/templates/form/comment_form.html:8
#: agenda/templates/form/comment_form.html:17
#: agenda/templates/form/participation_form.html:23
msgid "Post"
msgstr ""

#: agenda/templates/form/participation_form.html:8
msgid "I no longer participate"
msgstr ""

#: agenda/templates/form/participation_form.html:10
#: agenda/templates/form/participation_form.html:13
msgid "I participate !"
msgstr ""

#: agenda/templates/form/participation_form.html:20
msgid ""
"Add a way for us to contact you! (mail or phone). Only the organizer will be "
"able to see it"
msgstr ""

#: agenda/urls.py:14
msgid "^(?P<event_id>[0-9]+)/comments$"
msgstr ""

#: agenda/urls.py:19
msgid "^event$"
msgstr ""

#: agenda/urls.py:24
msgid "^event/(?P<pk>[\\d]+)$"
msgstr ""

#: agenda/urls.py:29
msgid "^comment$"
msgstr ""

#: agenda/urls.py:34
msgid "^participations$"
msgstr ""

#: agenda/urls.py:39
msgid "^ics$"
msgstr ""

#: agenda/urls.py:44
msgid "^event/update/(?P<event_id>[0-9]+)$"
msgstr ""

#: agenda/utils.py:29
msgid "New event"
msgstr ""
